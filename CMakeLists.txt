﻿# CMakeList.txt : CMake project for Opencl-MST, include source and define
# project specific logic here.

cmake_minimum_required (VERSION 3.8)
project(Opencl-MST)
#Adding Opencl libs and include files to the proj
find_package(OpenCL)
include_directories(${OpenCL_INCLUDE_DIRS})
link_directories(${OpenCL_LIBRARY})

#adding the Boost libs to the proj
if (WIN32)
    message("OS: Windows")
    set(BOOST_INC "C:/local/boost_1_76_0_b1_rc2")
    set(BOOST_LIB "C:/local/boost_1_76_0_b1_rc2/lib64-msvc-14.2/")

    include_directories(${BOOST_INC})
    link_directories(${BOOST_LIB})

elseif(UNIX)
    message("OS: Linux")
    include_directories(${Boost_INCLUDE_DIR})
endif()

#compile files in Core and OpenCL external libs
file(GLOB CORE_SRC "Core/*.cpp" "Core/*.c")
file(GLOB OPENCL_SRC "OpenCL/*.cpp" "OpenCL/*.c")
if (WIN32)
    file(GLOB BOOST_SRC "${BOOST_LIB}/*.lib")
endif()

if(MSVC)
    add_definitions(-D_CRT_SECURE_NO_WARNINGS)
endif()

# Add source to this project's executable.
add_executable (Opencl-MST "src/Main.cpp" ${CORE_SRC} ${OPENCL_SRC} src/KruskalCPU.cpp src/Graph.c src/InputLoader.h src/InputLoader.cpp src/KruskalCPU.h)

target_include_directories (Opencl-MST PUBLIC ${CMAKE_CURRENT_SOURCE_DIR} "CORE" "OPENCL" "src")
if(WIN32)
    target_link_libraries (Opencl-MST ${OpenCL_LIBRARY} imagehlp )
elseif(UNIX)
    target_link_libraries (Opencl-MST ${OpenCL_LIBRARY} dl boost_system)
endif()

# TODO: Add tests and install targets if needed.
