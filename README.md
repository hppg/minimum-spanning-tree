# Minimum Spanning Tree

This is a project of the course `High Performance Programming with Graphic Cards` to implement `Minimum spanning tree` algorithms on the CPU and GPU and compute the speedup on the GPU.


## Documentation of working on this project

Result of that documentation will be found [here](doc/documentation.pdf).

## Run

> When the project gets executed, results, time of computing and speedup will be shown in the terminal.  
> If the results of CPU and GPU are not equal, an error will be raised.

### Linux

```shell
mkdir build
cd build
cmake ..
make
./Opencl-MST
```

## Result

> You can see the resulting time of CPU and GPU implementations with the following specs in the following picture:  
> #### Specs:
>  - CPU: CPU: AMD Ryzen 5 2600 (six cores)
>  - RAM: 16GB
>  - GPU: NVIDIA GeForce RTX 3070
>  - VRAM: 16GB  
> 
> 
> ![result](doc/result.png)