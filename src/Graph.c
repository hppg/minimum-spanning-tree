#include <stdbool.h>
#include <malloc.h>
#include <stdio.h>
#include "Graph.h"

bool edgeContainsVertex(Edge* edge, unsigned int vertex) {
    return edge->vertex1 == vertex || edge->vertex2 == vertex;
}

unsigned int getOtherVertex(Edge* edge, unsigned int vertex) {
    if(edge->vertex1 == vertex) {
        return edge->vertex2;
    }
    if(edge->vertex2 == vertex) {
        return edge->vertex1;
    }
    printf("Error!\n");
}

void printEdges(const char* heading, Edge* edges, int countEdges) {
    printf("%s", heading);
    for (int i = 0; i < countEdges; i++) {
        printEdge(edges[i]);
    }
}

void printEdge(Edge edge) {
    printf("%u -> %u: %f\n", edge.vertex1, edge.vertex2, edge.weight);
}
