//
// Created by axherrm on 05.07.22.
//

#ifndef OPENCL_MST_GRAPH_H
#define OPENCL_MST_GRAPH_H

#include <stddef.h>
#define ARR_SIZE(x)  (sizeof(x) / sizeof((x)[0]))

/**
 * C99 data structures for graphs, vertices and edges.
 */

typedef struct {
    unsigned int vertex1;
    unsigned int vertex2;
    double weight;
} Edge;

typedef struct {
    Edge* edges;
    int edgeCount;
} KruskalResult;

bool edgeContainsVertex(Edge* edge, unsigned int vertex);

unsigned int getOtherVertex(Edge* edge, unsigned int vertex);

void printEdges(const char* heading, Edge* edges, int countEdges);

void printEdge(Edge edge);

#endif //OPENCL_MST_GRAPH_H