//
// Created by axherrm on 23.07.22.
//

#include <cstring>
#include <string>
#include <fstream>
#include <iostream>
#include "InputLoader.h"
#include <sstream>
#include <vector>
#include <algorithm>

extern "C"
{
#include "Graph.h"
}

using namespace std;

InputLoader::InputLoader(string path) {
    edgesCount = -1;
    edgesCPU = nullptr;
    edgesGPU = nullptr;
    loadInput(path);
}

void InputLoader::loadInput(string path) {
    ifstream stream;
    stream.open(path);
    string line;
    getline(stream, line);
    verticesCount = stoi(line);
    getline(stream, line);
    InputLoader::edgesCount = stoi(line);

    InputLoader::edgesCPU = new Edge[edgesCount];
    InputLoader::edgesGPU = new Edge[edgesCount];

    int i = 0;
    while(std::getline(stream, line)) {
        istringstream iss(line);
        unsigned int vertex1, vertex2;
        double weight;
        iss >> vertex1 >> vertex2 >> weight;

        edgesCPU[i].vertex1 = vertex1;
        edgesCPU[i].vertex2 = vertex2;
        edgesCPU[i].weight = weight;
        i++;
    }
    memcpy(&edgesGPU[0], &edgesCPU[0], sizeof(Edge) * edgesCount);
}

void InputLoader::free() {
    delete[] edgesCPU;
    delete[] edgesGPU;
}