//
// Created by axherrm on 23.07.22.
//

#ifndef OPENCL_MST_INPUTLOADER_H
#define OPENCL_MST_INPUTLOADER_H

#include <map>

extern "C"
{
    #include "Graph.h"
}

using namespace std;

class InputLoader {

public:
    InputLoader(string path);
    unsigned int edgesCount;
    unsigned int verticesCount;
    Edge* edgesCPU;
    Edge* edgesGPU;
    void loadInput(string path);
    void free();
};

#endif //OPENCL_MST_INPUTLOADER_H
