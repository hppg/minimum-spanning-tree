//
// Created by axherrm on 03.07.22.
//
#include "KruskalCPU.h"
#include <vector>
#include <algorithm>
#include <iostream>
#define byte unsigned char

extern "C"
{
    #include "Graph.h"
}

using namespace std;

UnionFind::UnionFind(unsigned int countVertices) {
    this->parent = new unsigned int[countVertices];
    this->rank = new byte[countVertices];
    for (unsigned int i = 0; i < countVertices; i++) {
        parent[i] = i;
        rank[i] = (unsigned char)0;
    }
}
UnionFind::~UnionFind() {
    delete[] parent;
    delete[] rank;
}
unsigned int UnionFind::find(unsigned int v) {
    while (v != parent[v]) {
        parent[v] = parent[parent[v]];    // path compression by halving
        v = parent[v];
    }
    return v;
}
void UnionFind::unionElements(unsigned int v1, unsigned int v2) {
    unsigned int root1 = find(v1);
    unsigned int root2 = find(v2);
    if(root1 == root2) {
        return;
    }
    if(rank[root1] > rank[root2]) {
        parent[root2] = root1;
    }
    else {
        parent[root1] = root2;
        if(rank[root1] == rank[root2]) {
            rank[root2] = (byte) (((int)rank[root2]) + 1);
        }
    }
}

/**
 * Main Part that computes the MST. For that Kruskal's algorithm is used together with union find sets to detect cycles.
 */
KruskalResult KruskalCPU::getMST(Edge* edges, int countEdges, unsigned int countVertices) {
    sort(edges, countEdges);
    Edge* result = new Edge[countVertices - 1];
    int resultCount = 0;
    UnionFind unionFind(countVertices);
    for (int i = 0; i < countEdges && resultCount < countVertices - 1; i++) {
        Edge* edge = &edges[i];
        if (unionFind.find(edge->vertex1) != unionFind.find(edge->vertex2)) {
            unionFind.unionElements(edge->vertex1, edge->vertex2);
            result[resultCount] = *edge;
            resultCount++;
        }
    }
    KruskalResult kruskalResult;
    kruskalResult.edges = result;
    kruskalResult.edgeCount = resultCount;
    return kruskalResult;
}
void KruskalCPU::sort(Edge* edges, int size) {
    merge_sort(0, size-1, edges, new Edge[size]);
}

// Merge sort algorithm, based on this code: https://hackr.io/blog/merge-sort-in-c
void KruskalCPU::merge_sort(int i, int j, Edge a[], Edge aux[]) {
    if (j <= i) {
        return;     // the subsection is empty or a single element
    }
    int mid = (i + j) / 2;
    merge_sort(i, mid, a, aux);     // sort the left sub-array recursively
    merge_sort(mid + 1, j, a, aux);     // sort the right sub-array recursively
    int pointer_left = i;       // pointer_left points to the beginning of the left sub-array
    int pointer_right = mid + 1;        // pointer_right points to the beginning of the right sub-array
    int k;      // k is the loop counter
    // we loop from i to j to fill each element of the final merged array
    for (k = i; k <= j; k++) {
        if (pointer_left == mid + 1) {      // left pointer has reached the limit
            aux[k] = a[pointer_right];
            pointer_right++;
        } else if (pointer_right == j + 1) {        // right pointer has reached the limit
            aux[k] = a[pointer_left];
            pointer_left++;
        } else if (a[pointer_left].weight < a[pointer_right].weight) {        // pointer left points to smaller element
            aux[k] = a[pointer_left];
            pointer_left++;
        } else {        // pointer right points to smaller element
            aux[k] = a[pointer_right];
            pointer_right++;
        }
    }
    for (k = i; k <= j; k++) {      // copy the elements from aux[] to a[]
        a[k] = aux[k];
    }
}

