//
// Created by axherrm on 29.07.22.
//

#ifndef OPENCL_MST_KRUSKALCPU_H
#define OPENCL_MST_KRUSKALCPU_H

extern "C"
{
#include "Graph.h"
}

using namespace std;

class UnionFind {
public:
    explicit UnionFind(unsigned int countVertices);
    ~UnionFind();
    unsigned int* parent;
    unsigned char* rank;
    unsigned int find(unsigned int v);
    void unionElements(unsigned int v1, unsigned int v2);
};

/**
 * Main Part that computes the MST. For that Kruskal's algorithm is used together with DFS to detect cycles.
 */
class KruskalCPU {
public:
    static KruskalResult getMST(Edge* edges, int countEdges, unsigned int countVertices);
private:
    void static sort(Edge* edges, int size);
    // Merge sort algorithm, based on this code: https://hackr.io/blog/merge-sort-in-c
    void static merge_sort(int i, int j, Edge a[], Edge aux[]);
};


#endif //OPENCL_MST_KRUSKALCPU_H
