#ifndef __OPENCL_VERSION__
#include <OpenCL/OpenCLKernel.hpp> // Hack to make syntax highlighting in Eclipse work
#endif

typedef struct {
    unsigned int vertex1;
    unsigned int vertex2;
    double weight;
} Edge;

// Method for debugging
void printEdge(Edge edge) {
    printf("%u -> %u: %f\n", edge.vertex1, edge.vertex2, edge.weight);
}

// Swap the values of edge1 and edge2
inline void swap(Edge *edge1, Edge *edge2) {
    Edge tmp;
    tmp = *edge2;
    *edge2 = *edge1;
    *edge1 = tmp;
}

// Sort edge1 and edge2 according to their weight
// If direction == 1 it will be ascending
inline void sort(Edge *edge1, Edge *edge2, char direction) {
    if ((edge1->weight > edge2->weight) == direction) swap(edge1, edge2);
}

// Swap the values of edge1 and edge2 just local
inline void swapLocal(__local Edge *edge1, __local Edge *edge2) {
    Edge tmp;
    tmp = *edge2;
    *edge2 = *edge1;
    *edge1 = tmp;
}

// Sort edge1 and edge2 according to their weight just local
// If direction == 1 it will be ascending
inline void sortLocal(__local Edge *edge1, __local Edge *edge2, char direction) {
    if ((edge1->weight > edge2->weight) == direction) swapLocal(edge1, edge2);
}

#define WG_SIZE 256
__kernel void startBitonicSortKernel(const __global Edge* input, __global Edge* output) {
    // Get necessary IDs
    const uint localId = get_local_id(0);
    const uint shortenedGlobalId = get_global_id(0) & (WG_SIZE - 1);
    const uint index = get_group_id(0) * (WG_SIZE * 2) + localId;

    __local Edge local_data[WG_SIZE * 2];

    // Load into local memory
    local_data[localId] = input[index];
    local_data[localId + WG_SIZE] = input[index + WG_SIZE];

    // Perform sorting
    for (uint blockSize = 2; blockSize <= WG_SIZE * 2; blockSize *= 2) {
        char direction = (shortenedGlobalId & (blockSize / 2)) == 0; // Sort the blocks in opposite directions
        // The biggest group needs to be sorted in a special direction
        if (blockSize == WG_SIZE * 2) {
            direction = (shortenedGlobalId & 0);
        }
#pragma unroll
        for (uint stepSize = blockSize / 2; stepSize > 0; stepSize /= 2) {
            barrier(CLK_LOCAL_MEM_FENCE);
            uint idx = 2 * localId - (localId & (stepSize - 1)); // Take every second block but starting neighbouring within one block
            sortLocal(&local_data[idx], &local_data[idx + stepSize], direction);
        }
    }

    // Synchronize kernels and write back to CPU
    barrier(CLK_LOCAL_MEM_FENCE);
    output[index] = local_data[localId];
    output[index + WG_SIZE] = local_data[localId + WG_SIZE];
}

__kernel void localBitonicSortKernel(__global Edge* data, const uint countEdges, const uint blockSize, uint stepSize) {
    // Get necessary IDs
    const uint localId = get_local_id(0);
    const uint shortenedGlobalId = get_global_id(0) & (countEdges / 2 - 1);
    const uint index = get_group_id(0) * (WG_SIZE * 2) + localId;

    __local Edge local_data[2 * WG_SIZE];

    // Load into local memory
    local_data[localId] = data[index];
    local_data[localId + WG_SIZE] = data[index + WG_SIZE];

    // Perform bitonic merge
    char direction = (shortenedGlobalId & (blockSize / 2)) == 0; // Sort the blocks in opposite directions
#pragma unroll
    for (uint stepSizeCopy = stepSize; stepSizeCopy > 0; stepSizeCopy /= 2) {
        barrier(CLK_LOCAL_MEM_FENCE);
        uint idx = 2 * localId - (localId & (stepSize - 1)); // Take every second block but starting neighbouring within one block
        sortLocal(&local_data[idx], &local_data[idx + stepSize], direction);
    }

    // Synchronize kernels and write back to CPU
    barrier(CLK_LOCAL_MEM_FENCE);
    data[index] = local_data[localId];
    data[index + WG_SIZE] = local_data[localId + WG_SIZE];
}

__kernel void globalBitonicSortKernel(__global Edge* data, const uint countEdges, const uint blockSize, const uint stepSize) {
    // Get necessary IDs
    uint shortenedGlobalId = get_global_id(0) & (countEdges / 2 - 1);
    uint index = 2 * shortenedGlobalId - (shortenedGlobalId & (stepSize - 1));

    // Sort
    char direction = (shortenedGlobalId & (blockSize / 2)) == 0;  // Sort the blocks in opposite directions
    Edge left = data[index];
    Edge right = data[index + stepSize];

    sort(&left, &right, direction);

    // Write back to CPU
    data[index] = left;
    data[index + stepSize] = right;
}

unsigned int find(unsigned int v, unsigned int* parent) {
    while (v != parent[v]) {
        parent[v] = parent[parent[v]];    // path compression by halving
        v = parent[v];
    }
    return v;
}

void unionElements(unsigned int v1, unsigned int v2, unsigned int* parent, unsigned char* rank) {
    unsigned int root1 = find(v1, parent);
    unsigned int root2 = find(v2, parent);
    if(root1 == root2) {
        return;
    }
    if(rank[root1] > rank[root2]) {
        parent[root2] = root1;
    }
    else {
        parent[root1] = root2;
        if(rank[root1] == rank[root2]) {
            rank[root2] = (unsigned char) (((int)rank[root2]) + 1);
        }
    }
}

__kernel void kruskalKernel(const __global Edge *input, __global Edge *output,__global unsigned int* sizeOfOutput, const uint countEdges,
                         const unsigned int countVertices, __global unsigned int *parent, __global unsigned char *rank) {
    unsigned int resultSize = 0;
    for (uint i = 0; i < countEdges && resultSize < countVertices - 1; i++) {
        if (find(input[i].vertex1, parent) != find(input[i].vertex2, parent)) {
            unionElements(input[i].vertex1, input[i].vertex2, parent, rank);
            output[resultSize] = input[i];
            resultSize++;
        }
    }
    *sizeOfOutput = resultSize;
}

__kernel void ascendingToDescendingKernel(const __global Edge *input, __global Edge *output, const uint countEdges) {
    if (input[0].weight > input[countEdges - 1].weight) {
        for (int i = 0; i < countEdges; ++i) {
            output[i] = input[countEdges - 1 - i];
        }
    } else {
        for (int i = 0; i < countEdges; ++i) {
            output[i] = input[i];
        }
    }
}