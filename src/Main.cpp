//////////////////////////////////////////////////////////////////////////////
// OpenCL Project: Minimum Spanning Tree
//////////////////////////////////////////////////////////////////////////////

// includes
#include <Core/Assert.hpp>
#include <Core/Time.hpp>
#include <OpenCL/cl-patched.hpp>
#include <OpenCL/Program.hpp>
#include <OpenCL/Event.hpp>
#include <OpenCL/Device.hpp>

#include <iostream>
#include <cmath>
#include "src/KruskalCPU.h"

#include <set>
#include <climits>
#include "src/InputLoader.h"

extern "C"
{
    #include "Graph.h"
}

using namespace std;

cl::CommandQueue queue;
cl::Program program;
cl::Context context;
string osPathPrefix;

// contains the most recent CPU and GPU results
double cpuResult;
double gpuResult;
Core::TimeSpan cpuTime = Core::TimeSpan(NULL);
Core::TimeSpan gpuTime = Core::TimeSpan(NULL);

int buildClProgram(int argc, char** argv) {
    // Create a context
    //cl::Context context(CL_DEVICE_TYPE_GPU);
    std::vector<cl::Platform> platforms;
    cl::Platform::get(&platforms);
    if (platforms.size() == 0) {
        std::cerr << "No platforms found" << std::endl;
        return 1;
    }
    int platformId = 0;
    for (size_t i = 0; i < platforms.size(); i++) {
        if (platforms[i].getInfo<CL_PLATFORM_NAME>() == "AMD Accelerated Parallel Processing") {
            platformId = i;
            break;
        }
    }
    cl_context_properties prop[4] = { CL_CONTEXT_PLATFORM, (cl_context_properties) platforms[platformId] (), 0, 0 };
    std::cout << "Using platform '" << platforms[platformId].getInfo<CL_PLATFORM_NAME>() << "' from '" << platforms[platformId].getInfo<CL_PLATFORM_VENDOR>() << "'" << std::endl;
    context = cl::Context(CL_DEVICE_TYPE_GPU, prop);

    // Get a device of the context
    int deviceNr = argc < 2 ? 1 : atoi(argv[1]);
    std::cout << "Using device " << deviceNr << " / " << context.getInfo<CL_CONTEXT_DEVICES>().size() << std::endl;
    ASSERT (deviceNr > 0);
    ASSERT ((size_t) deviceNr <= context.getInfo<CL_CONTEXT_DEVICES>().size());
    cl::Device device = context.getInfo<CL_CONTEXT_DEVICES>()[deviceNr - 1];
    std::vector<cl::Device> devices;
    devices.push_back(device);
    OpenCL::printDeviceInfo(std::cout, device);
    cout << endl;

    // Create a command queue
    queue = cl::CommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE);

    // Load the source code
    program = OpenCL::loadProgramSource(context, osPathPrefix + "../src/KruskalGPU.cl");
    // Compile the source code. This is similar to program.build(devices) but will print more detailed error messages
    OpenCL::buildProgram(program, devices);
    return 0;
}

void calculateOnCPU(Edge* edges, int countEdges, unsigned int countVertices) {
    cout << "CPU implementation of Kruskal's algorithm with Merge Sort starts." << endl;
    Core::TimeSpan beginTime = Core::getCurrentTime();
    KruskalResult outputCPUWithCount = KruskalCPU::getMST(edges, countEdges, countVertices);
    Edge* outputCPU = outputCPUWithCount.edges;
    int countOutputCPU = outputCPUWithCount.edgeCount;
    Core::TimeSpan endTime = Core::getCurrentTime();
    cpuTime = endTime - beginTime;
    cpuResult = 0;
    for (int i = 0; i < countOutputCPU; i++) {
        cpuResult += outputCPU[i].weight;
    }
}

void setOsPrefix() {
    #ifdef _WIN32
        cout << "Set OS prefix to Windows." << endl;
        osPathPrefix = "../";
    #elif _WIN64
        cout << "Set OS prefix to Windows." << endl;
        osPathPrefix = "../";
    #else
        cout << "Set OS prefix to Linux." << endl;
        osPathPrefix = "";
    #endif
}

void calculateOnGPU(Edge edgesInput[], unsigned int countEdgesInput, unsigned int countVertices) {
    cout << "CPU implementation of Kruskal's algorithm with Bitonic Sort starts." << endl;
    // Adjust the input so bitonic sort can handle it. It can only handle input sizes that are a power of 2.
    unsigned int countEdges = 2;
    while (countEdgesInput > countEdges) {
        countEdges *= 2;
    }
    Edge* edges = new Edge[countEdges];
    for (int i = 0; i < countEdges; i++) {
        if (i < countEdgesInput) {
            edges[i] = edgesInput[i];
        } else {
            // Add edges that have a huge weight, so they will appear at the end of the sorted array.
            // Kruskal should not reach them, but even if it does, they are ignored because they would add a circle.
            Edge maxWeightEdge;
            maxWeightEdge.vertex1 = 1;
            maxWeightEdge.vertex2 = 1;
            maxWeightEdge.weight = LONG_MAX;
            edges[i] = maxWeightEdge;
        }
    }

    // Declare workgroup sizes
    auto LOCAL_WG_SIZE = (unsigned int)256;
    size_t GLOBAL_WG_SIZE = (countEdges / 2) % LOCAL_WG_SIZE == 0 ? (countEdges / 2) : ((countEdges / 2) + LOCAL_WG_SIZE - ((countEdges / 2) % LOCAL_WG_SIZE));

    size_t sizeEdges = countEdges * sizeof(Edge);

    // Allocate space for in- and output data on the host
    std::vector<Edge> input (countEdges);
    std::vector<Edge> tmp1 (countEdges);
    std::vector<Edge> tmp2 (countEdges);
    unsigned int outputCount = 0;
    auto* parent = new unsigned int[countVertices];
    auto* rank = new unsigned char[countVertices];
    // parent and rank need to have values, otherwise the kernel fails
    for(unsigned int i = 0; i < countVertices; i++) {
        parent[i] = i;
        rank[i] = 0;
    }

    // Allocate space for in- and output data on the device
    cl::Buffer inputBuffer (context, CL_MEM_READ_WRITE, sizeEdges);
    cl::Buffer tmpBuffer1 (context, CL_MEM_READ_WRITE, sizeEdges);
    cl::Buffer tmpBuffer2 (context, CL_MEM_READ_WRITE, sizeEdges);
    cl::Buffer outputBuffer (context, CL_MEM_READ_WRITE, sizeEdges);
    cl::Buffer outputCountBuffer (context, CL_MEM_READ_WRITE, sizeof(outputCount));
    cl::Buffer parentBuffer (context, CL_MEM_READ_WRITE, countVertices * sizeof(unsigned int));
    cl::Buffer rankBuffer (context, CL_MEM_READ_WRITE, countVertices * sizeof(unsigned char));

    // Initialize memory to 0xff (useful for debugging because otherwise GPU memory will contain information from last execution)
    memset(input.data(), 255, sizeEdges);
    memset(tmp1.data(), 255, sizeEdges);
    memset(tmp2.data(), 255, sizeEdges);

    // Link
    queue.enqueueWriteBuffer(inputBuffer, true, 0, sizeEdges, input.data());
    queue.enqueueWriteBuffer(tmpBuffer1, true, 0, sizeEdges, tmp1.data());
    queue.enqueueWriteBuffer(tmpBuffer2, true, 0, sizeEdges, tmp2.data());

    // Create kernels
    cl::Kernel startBitonicSortKernel(program, "startBitonicSortKernel");
    cl::Kernel globalBitonicSortKernel(program, "globalBitonicSortKernel");
    cl::Kernel localBitonicSortKernel(program, "localBitonicSortKernel");
    cl::Kernel ascendingToDescendingKernel(program, "ascendingToDescendingKernel");
    cl::Kernel kruskalKernel(program, "kruskalKernel");

    // Create events to measure the time
    cl::Event writeEvent;
    cl::Event readEvent[2];
    vector<cl::Event> kernelEvents;

    // Copy input data to device
    queue.enqueueWriteBuffer(inputBuffer, true, 0, sizeEdges, edges, nullptr, &writeEvent);

    // Call startBitonicSortKernel
    startBitonicSortKernel.setArg<cl::Buffer>(0, inputBuffer);
    startBitonicSortKernel.setArg<cl::Buffer>(1, tmpBuffer1);

    cl::Event kernelEvent1;
    queue.enqueueNDRangeKernel(startBitonicSortKernel, cl::NullRange, GLOBAL_WG_SIZE, LOCAL_WG_SIZE, nullptr, &kernelEvent1);
    kernelEvents.push_back(kernelEvent1);

    // Call local- and globalBitonicSortKernel
    // This is necessary, because the startBitonicSortKernel can only correctly handle a limited number of inputs
    for (unsigned int blockSize = 2 * LOCAL_WG_SIZE; blockSize <= countEdges; blockSize *= 2) {
        for (unsigned int stepSize = blockSize / 2; stepSize > 0; stepSize /= 2) {
            if (stepSize >= 2 * LOCAL_WG_SIZE) {
                // sort global
                globalBitonicSortKernel.setArg<cl::Buffer>(0, tmpBuffer1);
                globalBitonicSortKernel.setArg(1, countEdges);
                globalBitonicSortKernel.setArg(2, blockSize);
                globalBitonicSortKernel.setArg(3, stepSize);

                cl::Event kernelEvent2;
                queue.enqueueNDRangeKernel(globalBitonicSortKernel, cl::NullRange, GLOBAL_WG_SIZE, LOCAL_WG_SIZE, nullptr, &kernelEvent2);
                kernelEvents.push_back(kernelEvent2);
            } else {
                // sort local
                localBitonicSortKernel.setArg<cl::Buffer>(0, tmpBuffer1);
                localBitonicSortKernel.setArg(1, countEdges);
                localBitonicSortKernel.setArg(2, blockSize);
                localBitonicSortKernel.setArg(3, stepSize);

                cl::Event kernelEvent3;
                queue.enqueueNDRangeKernel(localBitonicSortKernel, cl::NullRange, GLOBAL_WG_SIZE, LOCAL_WG_SIZE, nullptr, &kernelEvent3);
                kernelEvents.push_back(kernelEvent3);
            }
        }
    }

    clWaitForEvents(kernelEvents.size(), reinterpret_cast<cl_event const *>(kernelEvents.data()));

    ascendingToDescendingKernel.setArg<cl::Buffer>(0, tmpBuffer1);
    ascendingToDescendingKernel.setArg<cl::Buffer>(1, tmpBuffer2);
    ascendingToDescendingKernel.setArg<unsigned int>(2, countEdges);

    cl::Event kernelEvent4;
    queue.enqueueTask(ascendingToDescendingKernel, nullptr, &kernelEvent4);
    kernelEvents.push_back(kernelEvent4);

    clWaitForEvents(1, reinterpret_cast<cl_event const *>(&kernelEvent4));

    queue.enqueueWriteBuffer(parentBuffer, true, 0, countVertices * sizeof(unsigned int), parent);
    queue.enqueueWriteBuffer(rankBuffer, true, 0, countVertices * sizeof(unsigned char), rank);

    // Call Kruskal kernel
    kruskalKernel.setArg<cl::Buffer>(0, tmpBuffer2);
    kruskalKernel.setArg<cl::Buffer>(1, outputBuffer);
    kruskalKernel.setArg<cl::Buffer>(2, outputCountBuffer);
    kruskalKernel.setArg(3, countEdges);
    kruskalKernel.setArg(4, countVertices);
    kruskalKernel.setArg<cl::Buffer>(5, parentBuffer);
    kruskalKernel.setArg<cl::Buffer>(6, rankBuffer);

    cl::Event kernelEvent5;
    queue.enqueueTask(kruskalKernel, nullptr, &kernelEvent5);
    kernelEvents.push_back(kernelEvent5);
    queue.enqueueReadBuffer(outputCountBuffer, true, 0, sizeof(unsigned int), &outputCount, nullptr, &readEvent[0]);
    std::vector<Edge> output (outputCount);
    queue.enqueueReadBuffer(outputBuffer, true, 0, outputCount * sizeof(Edge), output.data(), nullptr, &readEvent[1]);

    gpuResult = 0;
    for (const auto &edge: output) {
        gpuResult += edge.weight;
    }

    // Calculate time on GPU
    gpuTime = OpenCL::getElapsedTime(writeEvent);
    gpuTime = gpuTime + OpenCL::getElapsedTime(readEvent[0]);
    gpuTime = gpuTime + OpenCL::getElapsedTime(readEvent[1]);
    for (const cl::Event& kernelEvent : kernelEvents) {
        gpuTime = gpuTime + OpenCL::getElapsedTime(kernelEvent);
    }
}

/**
 * Performs computation of the same input on the CPU and on the GPU and compares the results.
 * If any result is different, an error is raised.
 * @param inputFilePath
 */
void performComparison(const string& inputFilePath) {
    cout << "Loading data file \"" << inputFilePath << "\"." << endl;
    auto* inputLoader = new InputLoader(inputFilePath);
    cout << "Starting comparison of CPU and GPU implementation for data file \"" << inputFilePath << "\"." << endl;
    calculateOnCPU(inputLoader->edgesCPU, inputLoader->edgesCount, inputLoader->verticesCount);
    calculateOnGPU(inputLoader->edgesGPU, inputLoader->edgesCount, inputLoader->verticesCount);
    cout << "CPU time: " << cpuTime << endl;
    cout << "GPU time: " << gpuTime << endl;
    cout << "GPU speedup: " << cpuTime.getSeconds() / gpuTime.getSeconds() << endl;
    cout << "CPU result: " << cpuResult << endl;
    cout << "GPU result: " << gpuResult << endl;
    if(cpuResult == gpuResult) {
        cout << "Results are equal." << endl;
    } else {
        cout << "Results are not equal." << endl;
        ASSERT(cpuResult == gpuResult);
    }
    cout << endl << endl;
    inputLoader->free();
}

//////////////////////////////////////////////////////////////////////////////
// Main function
//////////////////////////////////////////////////////////////////////////////
int main(int argc, char** argv) {
    cout << endl << "Project: Minimum Spanning Tree algorithm" << endl;
    cout << "Course: High Performance Programming with Graphic Cards" << endl << endl;

    // Linux and Windows seem to interpret relative paths differently. Windows needs additional "../" in front of the path.
    setOsPrefix();

    if(buildClProgram(argc, argv) == 1) {
        return 1;
    }

    performComparison(osPathPrefix + "../resources/wikipediaExample.txt");
    performComparison(osPathPrefix + "../resources/tinyEWG.txt");
    performComparison(osPathPrefix + "../resources/mediumEWG.txt");
    performComparison(osPathPrefix + "../resources/1000EWG.txt");
    performComparison(osPathPrefix + "../resources/10000EWG.txt");
    performComparison(osPathPrefix + "../resources/largeEWG.txt");

    // return 0 to mark as successful
	return 0;
}
